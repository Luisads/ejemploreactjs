import React, { Component } from 'react';

export default class TextFiles extends Component {
  render () {
    return (
    <div>
      <label> {this.props.label} </label>
      <input type={this.props.type} name={this.props.name} />
    </div>
    )
  }
}
