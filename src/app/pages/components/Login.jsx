import React from 'react';
import TextFiles from './LogComponents/TextFiles.jsx';
import Button from './LogComponents/Buttons.jsx';

export default class Login extends React.Component {
  render () {
    return (
    <div>
      <TextFiles label="Usuario" type="text" name="usuario"/>
      <br />
      <TextFiles label="Contraseña" type="password" name="password" />
      <br />
      <Button type="button" name="Enviar" />
    </div>
    )
  }
}

/*const handleSubmit= function(e){
  //previene el hecho de que la pagina se recarge a cada momento
  e.preventDefault();
}*/
