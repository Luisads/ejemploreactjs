import React from 'react';
import Login from './components/Login.jsx';
import Header from './components/Header.jsx';
import Footer from './components/Footer.jsx';

export default class Layout extends React.Component {
  render () {
    return (
      <div>
        <Header />
        <h1>Holas!!!</h1>
        <Login />
        <Footer />
      </div>
    )
  }
}
