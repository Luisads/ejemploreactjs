import React from 'react';
import ReactDom from 'react-dom';
import Layout from './pages/Layout.jsx';

var app = document.getElementById('app');

ReactDom.render(<Layout />, app);
