var path = require('path');

module.exports = {
  entry: path.resolve(__dirname, 'src') + '/app/index.js',
  output: {
    path: path.resolve(__dirname, 'dist') + '/server',
    filename: 'bundle.js',
    publicPath: '/server/',
  },
  module: {
      loaders: [
        {
         test: /\.json$/,
        loader: 'json-loader',
       },
        {
        test: /\.jsx?$/,
        include: path.resolve(__dirname, 'src'),
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015', 'latest-minimal']
        }
      },
      {
      test: /\.css$/,
      loader: 'style-loader!css-loader',
      }
      ]
  }
}
